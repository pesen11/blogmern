import React from "react";
import { MDBFooter } from "mdb-react-ui-kit";

export default function FooterComponent() {
  return (
    <MDBFooter bgColor="light" className="text-center text-lg-left">
      <div className="text-center p-3" style={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}>
        &copy; {new Date().getFullYear()} Copyright: Hemant Gurung (ghemantrung7@gmail.com)
      </div>
    </MDBFooter>
  );
}
