import { useEffect, useState, useCallback } from "react";
import { Container } from "react-bootstrap";
import { getAllUsers } from "../../services/userServices";

import AuthorPagination from "./Pagination/AuthorPagination";

const AllAuthorPage = () => {
  const [users, setUsers] = useState();

  const getUsers = useCallback(async () => {
    try {
      let result = await getAllUsers();

      if (result) {
        //prettier-ignore
        // let showUser = result.slice[0,5];

        setUsers(result);
      }
    } catch (err) {
      console.log(err);
    }
  }, []);

  useEffect(() => {
    getUsers();
  }, [getUsers]);

  return (
    <>
      {users && (
        <>
          <Container>
            <h1 className="text-center">All Authors</h1>
            <hr />
            <AuthorPagination users={users} />
          </Container>
        </>
      )}
    </>
  );
};

export default AllAuthorPage;
