import { useEffect, useState } from "react";
import { getAllPosts } from "../../services/postServices";
import { Container, Row, Col } from "react-bootstrap";
import PostPagination from "./Pagination/PostPagination";
import "../../assets/css/pagination.css";

const PostsPage = () => {
  const [posts, setPosts] = useState();

  const getPosts = async () => {
    try {
      let allPosts = await getAllPosts();
      if (allPosts) {
        setPosts(allPosts);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <>
      <Container>
        {posts && posts.length ? (
          <>
            <Row className="mt-3">
              <Col>
                <h1 className="text-center">All Posts</h1>
                <hr />
              </Col>
            </Row>
            <PostPagination posts={posts} />
          </>
        ) : (
          <>
            <Row className="mt-3">
              <Col>
                <h1 className="text-center">You have not written any posts.</h1>
                <hr />
              </Col>
            </Row>
          </>
        )}
      </Container>
    </>
  );
};

export default PostsPage;
