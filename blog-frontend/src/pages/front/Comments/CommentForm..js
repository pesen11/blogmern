import { Form, Button } from "react-bootstrap";
import { useFormik } from "formik";
import * as Yup from "yup";

const CommentForm = ({ handleSubmit }) => {
  const commentValidation = Yup.object().shape({
    comment: Yup.string().required("Comment cannot be empty"),
  });

  const formik = useFormik({
    initialValues: {
      comment: "",
    },
    validationSchema: commentValidation,
    onSubmit: async (values, { resetForm }) => {
      handleSubmit(values);
      resetForm({ values: "" });
    },
  });
  return (
    <>
      <Form onSubmit={formik.handleSubmit}>
        <Form.Group className="mb-2 ">
          <Form.Label htmlFor="comment">Comment</Form.Label>
          <Form.Control
            size="lg"
            type="text-area"
            placeholder="Your response"
            name="comment"
            required={true}
            onChange={formik.handleChange}
            value={formik.values.comment}
          />
          {formik.errors.comment && <em className="text-danger">{formik.errors.comment}</em>}
        </Form.Group>

        <Button variant="success" type="submit">
          Comment
        </Button>
      </Form>
    </>
  );
};

export default CommentForm;
