import { useCallback, useEffect, useState } from "react";
import { addComment, getCommentsByPost } from "../../../services/commentServices";
import CommentCard from "./CommentCard";
import { Col } from "react-bootstrap";
import CommentForm from "./CommentForm.";
import { deleteCommentByIdForPost } from "../../../services/commentServices";
import { toast } from "react-toastify";

const CommentSection = ({ postId }) => {
  let localUser = JSON.parse(localStorage.getItem("authUser")) ?? null;
  const [comments, setComments] = useState();
  let pId = postId;

  const getAllComments = useCallback(async () => {
    try {
      let result = await getCommentsByPost(postId);

      setComments(result.result);
    } catch (err) {
      console.log(err);
    }
  }, [postId]);

  const deleteComment = async (postId, commentId) => {
    try {
      let response = await deleteCommentByIdForPost(postId, commentId);

      if (response.status) {
        getAllComments();
      }
    } catch (err) {
      console.log(err);
    }
  };

  const handleSubmit = async (data) => {
    try {
      // console.log(data, postId);
      if (localUser) {
        let result = await addComment(data, postId);
        if (result.status) {
          getAllComments();
        }
      } else {
        toast.error("Sign Up to Comment");
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getAllComments();
  }, [getAllComments]);

  return (
    <>
      <CommentForm handleSubmit={handleSubmit} />
      <div className="mb-5"></div>
      {comments &&
        comments.map((item, index) => (
          <Col sm={12} key={index}>
            {/* <SingleProductView data={item} type={"product"} /> */}
            <CommentCard comment={item} postId={pId} deleteFunc={deleteComment} />
            <hr />
          </Col>
        ))}
    </>
  );
};

export default CommentSection;
