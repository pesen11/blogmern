import { Container, Row, Col, Image } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import ActionButtons from "../../../components/common/actionButto";

const CommentCard = ({ comment, postId, deleteFunc }) => {
  let localUser = JSON.parse(localStorage.getItem("authUser")) ?? null;
  const isOC = () => {
    if (localUser) {
      return comment.user._id === localUser._id;
    } else {
      return false;
    }
  };

  const del = () => {
    deleteFunc(postId, comment._id);
  };
  return (
    <>
      <Container>
        <Row>
          <Col sm={12} md={1}>
            <Image
              src={comment.user.image}
              thumbnail
              roundedCircle
              style={{ height: "48px", width: "48px", border: "none" }}
            ></Image>
          </Col>
          <Col>
            <NavLink to={"/profile/@" + comment.user.name} className="nav-link">
              <h6>{comment.user.name}</h6>
            </NavLink>
            <Row>
              <Col>
                <p>{comment.comment}</p>
              </Col>
              {isOC() && (
                <>
                  <Col sm={1}>
                    <ActionButtons id={comment._id} onDeleteClick={del} postId={postId} />
                  </Col>
                </>
              )}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default CommentCard;
