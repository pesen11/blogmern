import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import { MDBRow, MDBCol } from "mdb-react-ui-kit";
import PostCard from "../../../components/common/PostCard";

const PostPagination = ({ posts }) => {
  const data = posts;

  // We start with an empty list of items.
  const [currentItems, setCurrentItems] = useState();
  const [pageCount, setPageCount] = useState(0);
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0);
  const itemsPerPage = 4;

  useEffect(() => {
    // Fetch items from another resources.
    const endOffset = itemOffset + itemsPerPage;

    setCurrentItems(data.slice(itemOffset, endOffset));
    setPageCount(Math.ceil(data.length / itemsPerPage));
  }, [itemOffset, itemsPerPage, data]);

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % data.length;
    setItemOffset(newOffset);
  };

  return (
    <>
      <MDBRow className="row-cols-1 row-cols-md-2 g-4">
        {currentItems &&
          currentItems.map((item, index) => (
            <MDBCol key={index} className="mb-5">
              <PostCard data={item} type="post" />
            </MDBCol>
          ))}
      </MDBRow>
      <ReactPaginate
        breakLabel="..."
        nextLabel="next >"
        onPageChange={handlePageClick}
        pageRangeDisplayed={3}
        pageCount={pageCount}
        previousLabel="< previous"
        renderOnZeroPageCount={null}
        containerClassName="pagination"
        pageLinkClassName="page-num"
        previousLinkClassName="page-num"
        nextLinkClassName="page-num"
        activeLinkClassName="active"
      />
    </>
  );
};

export default PostPagination;
