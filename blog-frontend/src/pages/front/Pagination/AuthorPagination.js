import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import { Row, Col, Image } from "react-bootstrap";
// import PostCard from "../../../components/common/PostCard";
import { NavLink } from "react-router-dom";

const AuthorPagination = ({ users }) => {
  const data = users;

  // We start with an empty list of items.
  const [currentItems, setCurrentItems] = useState();
  const [pageCount, setPageCount] = useState(0);
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0);
  const itemsPerPage = 12;

  useEffect(() => {
    // Fetch items from another resources.
    const endOffset = itemOffset + itemsPerPage;

    setCurrentItems(data.slice(itemOffset, endOffset));
    setPageCount(Math.ceil(data.length / itemsPerPage));
  }, [itemOffset, itemsPerPage, data]);

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % data.length;
    setItemOffset(newOffset);
  };

  return (
    <>
      <Row className="mt-5">
        {currentItems &&
          currentItems.map((user, index) => (
            <Col xs={6} sm={4} md={3} key={index}>
              <NavLink to={"/profile/@" + user.name} className="nav-link">
                <Image
                  src={user.image}
                  thumbnail
                  roundedCircle
                  style={{ height: "160px", width: "160px", border: "none" }}
                  className="mx-auto d-block"
                ></Image>
                <p className="text-center" style={{ fontSize: "25px" }}>
                  {user.name}
                </p>
              </NavLink>
            </Col>
          ))}
      </Row>
      <ReactPaginate
        breakLabel="..."
        nextLabel="next >"
        onPageChange={handlePageClick}
        pageRangeDisplayed={3}
        pageCount={pageCount}
        previousLabel="< previous"
        renderOnZeroPageCount={null}
        containerClassName="pagination"
        pageLinkClassName="page-num"
        previousLinkClassName="page-num"
        nextLinkClassName="page-num"
        activeLinkClassName="active"
      />
    </>
  );
};

export default AuthorPagination;
