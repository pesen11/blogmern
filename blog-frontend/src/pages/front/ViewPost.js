import { getPostBySlug, toggleStar } from "../../services/postServices";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { useState, useEffect, useCallback } from "react";
import { Container, Row, Col, Image, Button } from "react-bootstrap";
import "../../assets/css/ckStyles.css";
// import CommentForm from "./Comments/CommentForm.";
import CommentSection from "./Comments/CommentSection";
import { useContext } from "react";
import { UserContext } from "../../router/routes";
import { toast } from "react-toastify";

const ViewPost = () => {
  const params = useParams();
  const [post, setPost] = useState();
  const { dispatchStar } = useContext(UserContext);
  let localUser = JSON.parse(localStorage.getItem("authUser")) ?? null;
  const { starState } = useContext(UserContext);
  let navigate = useNavigate();

  const getPost = useCallback(async () => {
    try {
      let post = await getPostBySlug(params.slug);
      let currentUser = JSON.parse(localStorage.getItem("authUser")) ?? null;

      setPost(post.result);
      // console.log(post.result.stars[0].userId, localUser.id);
      if (currentUser) {
        let starUser = post.result.stars.filter((stars) => stars.userId === currentUser._id);

        if (starUser.length > 0) {
          dispatchStar({ type: "STAR", payload: true });
        } else {
          dispatchStar({ type: "STAR", payload: false });
        }
      }
    } catch (err) {
      console.log(err);
      navigate("/404");
    }
  }, [params, dispatchStar, navigate]);

  const addStar = async () => {
    try {
      if (localUser) {
        let result = await toggleStar(post._id);
        if (result.status) {
          getPost();
          // dispatchStar({ type: "STAR", payload: post._id });
        }
      } else {
        toast.error("Sign up to contribute a star.");
      }
    } catch (err) {
      console.log(err);
    }
  };

  const ButtonStr = () => {
    if (localUser) {
      if (starState) {
        return (
          <>
            <Button onClick={addStar}>Unstar Post</Button>
          </>
        );
      } else {
        return (
          <>
            <Button onClick={addStar}>Star Post</Button>
          </>
        );
      }
    } else {
      return (
        <>
          <Button onClick={addStar}>Star Post</Button>
        </>
      );
    }
  };

  useEffect(() => {
    getPost();
  }, [getPost]);

  return (
    <>
      <Container className="ck-content">
        {post && (
          <>
            <Row>
              <Col lg={1}>
                <Image
                  src={post.author.image}
                  thumbnail
                  roundedCircle
                  style={{ height: "90px", width: "90px", border: "none" }}
                ></Image>
              </Col>
              <Col lg={11}>
                <h5 className="mt-2">
                  <NavLink
                    to={"/profile/@" + post.author.name}
                    className="nav-link"
                    style={{ color: "blue" }}
                  >
                    {post.author.name}
                  </NavLink>
                </h5>
                <Row className="mt-2">
                  <Col>
                    <h5>{post.date}</h5>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row className="mt-5">
              <h1>{post.title}</h1>
              <hr />
              <Col dangerouslySetInnerHTML={{ __html: post.content }} className="mt-5"></Col>
            </Row>
            <Row>
              <Col xs={2}>
                <h2>
                  <i className="fa fa-star">{post.stars.length}</i>
                </h2>
              </Col>
              <Col xs={10}>
                <ButtonStr />
              </Col>
            </Row>
            <hr />

            <CommentSection postId={post._id} />
          </>
        )}
      </Container>
    </>
  );
};

export default ViewPost;
