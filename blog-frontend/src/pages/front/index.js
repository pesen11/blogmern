import HomePage from "./HomePage";
import RegisterPage from "./RegisterPage";
import LoginPage from "./LoginPage";
import PostsPage from "./PostsPage";
import ViewPost from "./ViewPost";
import ProfilePage from "./ProfilePage";
import NotFoundPage from "./NotFoundPage";
import AllAuthorPage from "./AllAuthorPage";

export const Front = {
  HomePage,
  RegisterPage,
  LoginPage,
  PostsPage,
  ViewPost,
  ProfilePage,
  NotFoundPage,
  AllAuthorPage,
};
