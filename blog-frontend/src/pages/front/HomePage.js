import { MDBRow, MDBCol } from "mdb-react-ui-kit/dist/mdb-react-ui-kit";
import { useEffect, useState, useCallback } from "react";
import { Container, Row, Col, Button, Image } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import "../../assets/css/homeStyle.css";
import PostCard from "../../components/common/PostCard";
import FooterComponent from "../../front/FooterComponent";
import { getAllPosts } from "../../services/postServices";
import { getAllUsers } from "../../services/userServices";

const HomePage = () => {
  const [posts, setPosts] = useState();
  const [users, setUsers] = useState();

  const getPosts = useCallback(async () => {
    try {
      let result = await getAllPosts();
      if (result) {
        let showPost = result.slice(0, 6);
        setPosts(showPost);
      }
    } catch (err) {
      console.log(err);
    }
  }, []);

  const getUsers = useCallback(async () => {
    try {
      let result = await getAllUsers();

      if (result) {
        //prettier-ignore
        // let showUser = result.slice[0,5];
        let showUser=result.slice(0,4)
        setUsers(showUser);
      }
    } catch (err) {
      console.log(err);
    }
  }, []);

  useEffect(() => {
    getPosts();
    getUsers();
  }, [getPosts, getUsers]);
  return (
    <>
      <div className="bg-warning homeTitles">
        <Container className="mb-10">
          <Row>
            <Col sm={12} md={6}>
              <h1 className="mt-5 mb-5 mainTitle">Express It!</h1>
              <h5 className="subTitle">Express your story, experiences and look through what </h5>
              <h5 className="mb-4 subTitle">people have in their mind.</h5>
              <h1 className="mb-4">Let's get this started.</h1>
            </Col>
            <Col sm={0} md={6}></Col>
          </Row>
        </Container>
      </div>
      <hr />

      <Container className="mt-5">
        <Row className="mb-2">
          <Col xs={2} md={2}>
            <h1>Authors</h1>
          </Col>
          <Col xs={{ span: 4, offset: 6 }} md={{ span: 2, offset: 8 }}>
            <NavLink to={"/allAuthors"}>
              <Button variant="success">View All</Button>
            </NavLink>
          </Col>
        </Row>

        <Row>
          {users &&
            users.map((user, index) => (
              <Col xs={6} sm={4} md={3} key={index}>
                <NavLink to={"/profile/@" + user.name} className="nav-link">
                  <Image
                    src={user.image}
                    thumbnail
                    roundedCircle
                    style={{ height: "160px", width: "160px", border: "none" }}
                    className="mx-auto d-block"
                  ></Image>
                  <p className="text-center" style={{ fontSize: "25px" }}>
                    {user.name}
                  </p>
                </NavLink>
              </Col>
            ))}
        </Row>
      </Container>
      <hr />

      <Container className="mt-5 mb-10">
        <Row className="mb-2">
          <Col xs={2} md={2}>
            <h1>Posts</h1>
          </Col>
          <Col xs={{ span: 4, offset: 6 }} md={{ span: 2, offset: 8 }}>
            <NavLink to={"/posts"}>
              <Button variant="success">View All</Button>
            </NavLink>
          </Col>
        </Row>

        <MDBRow className="row-cols-1 row-cols-md-2 g-4">
          {posts &&
            posts.map((item, index) => (
              <MDBCol key={index}>
                <PostCard data={item} type="post" />
              </MDBCol>
            ))}
        </MDBRow>
      </Container>

      <hr />

      <div>
        <FooterComponent />
      </div>
    </>
  );
};

export default HomePage;
