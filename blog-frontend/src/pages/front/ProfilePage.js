import { useCallback, useContext, useEffect, useState } from "react";
import { Container, Row, Image, Col, Button } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import {
  followUserById,
  getUserByNameService,
  unFollowUserById,
} from "../../services/userServices";

import { UserContext } from "../../router/routes";
import { toast } from "react-toastify";
import PostPagination from "./Pagination/PostPagination";

const ProfilePage = () => {
  const params = useParams();
  const [user, setUser] = useState();
  const { followState, dispatchFollow } = useContext(UserContext);
  let localUser = JSON.parse(localStorage.getItem("authUser")) ?? null;
  let navigate = useNavigate();

  const getUserByName = useCallback(async () => {
    try {
      let user = await getUserByNameService(params.authorName);
      let currentUser = JSON.parse(localStorage.getItem("authUser")) ?? null;
      if (user) {
        setUser(user);
        if (currentUser) {
          let followedByCurrentUser = user.followers.filter(
            (follower) => follower._id === currentUser._id
          );

          if (followedByCurrentUser.length) {
            dispatchFollow({ type: "FOLLOW", payload: true });
          } else {
            dispatchFollow({ type: "FOLLOW", payload: false });
          }
        }
      }

      //   console.log(user);
    } catch (err) {
      console.log(err);
      navigate("/404");
    }
  }, [dispatchFollow, params, navigate]);

  const followUser = async () => {
    try {
      if (localUser) {
        let user = await getUserByNameService(params.authorName);
        if (user) {
          await followUserById(user._id);
          getUserByName();
        }
      } else {
        toast.error("Sign up to follow.");
      }
    } catch (err) {
      console.log(err);

      toast.error(err.response.data.msg);
    }
  };

  const unFollowUser = async () => {
    try {
      if (localUser) {
        let user = await getUserByNameService(params.authorName);
        if (user) {
          await unFollowUserById(user._id);
          getUserByName();
        }
      } else {
        toast.error("Cannot");
      }
    } catch (err) {
      console.log(err);
    }
  };

  const FollowBtn = () => {
    if (localUser) {
      if (followState) {
        return (
          <>
            <Button className="mt-3" onClick={unFollowUser}>
              Unfollow
            </Button>
          </>
        );
      } else {
        return (
          <>
            <Button className="mt-3" onClick={followUser}>
              Follow
            </Button>
          </>
        );
      }
    } else {
      return (
        <>
          <Button className="mt-3" onClick={followUser}>
            Follow
          </Button>
        </>
      );
    }
  };

  useEffect(() => {
    getUserByName();
  }, [params, getUserByName]);

  return (
    <>
      <Container className="mt-2">
        {user && (
          <>
            <Row>
              <Col xs={3} lg={1}>
                <Image
                  src={user.image}
                  thumbnail
                  roundedCircle
                  style={{ height: "90px", width: "90px", border: "none" }}
                ></Image>
              </Col>
              <Col xs={9} lg={11}>
                <Row className="mb-3">
                  <Col xs={4} md={2} lg={2}>
                    <h3 className="mt-3">{user.name}</h3>
                  </Col>
                  <Col xs={4} md={3} lg={2}>
                    <FollowBtn />
                  </Col>
                </Row>

                <Row>
                  <Col xs={4} md={2}>
                    <h6>{user.posts.length} Posts</h6>
                  </Col>
                  <Col xs={4} md={2}>
                    <h6>{user.followers.length} Followers</h6>
                  </Col>
                  <Col xs={4} md={2}>
                    <h6>{user.following.length} Following</h6>
                  </Col>
                </Row>
              </Col>
            </Row>
            <hr />
            <p>
              <b>About Me:</b>
            </p>
            <p>{user.about}</p>
            <hr />
            <h3>User Posts</h3>
            {user.posts && user.posts.length ? (
              <>
                {/* <MDBRow className="row-cols-1 row-cols-md-2 g-4">
                  {user.posts.map((item, index) => (
                    <MDBCol key={index}>
                      <PostCard data={item} type="post" />
                    </MDBCol>
                  ))}
                </MDBRow> */}
                <PostPagination posts={user.posts} />
              </>
            ) : (
              <>
                <p>This user has yet to write any posts.</p>
              </>
            )}
          </>
        )}
      </Container>
    </>
  );
};

export default ProfilePage;
