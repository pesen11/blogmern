import API_ENDPOINTS from "../config/apiEndPoints";

import { postRequest, getRequest, deleteRequest } from "./axiosService";

export const getCommentsByPost = async (postId) => {
  try {
    let comments = await getRequest(API_ENDPOINTS.COMMENT + "/" + postId, false);
    return comments;
  } catch (err) {
    throw err;
  }
};

export const addComment = async (data, postId) => {
  try {
    let response = await postRequest(API_ENDPOINTS.COMMENT + "/" + postId, data, true, false);
    return response;
  } catch (err) {
    throw err;
  }
};

export const deleteCommentByIdForPost = async (postId, commentId) => {
  try {
    let result = await deleteRequest(API_ENDPOINTS.COMMENT + "/" + postId + "/" + commentId, true);
    return result;
  } catch (err) {
    throw err;
  }
};
