import API_ENDPOINTS from "../config/apiEndPoints";

import { postRequest, getRequest } from "./axiosService";

export const getUserByNameService = async (authorName) => {
  try {
    let response = await getRequest(API_ENDPOINTS.USER + "/byname/" + authorName);
    return response.result;
  } catch (err) {
    throw err;
  }
};

// prettier-ignore
export const followUserById = async (followId) => {
  try {
    await postRequest(API_ENDPOINTS.USER + "/follow/" + followId,{},true,false);
    // console.log(response);
    
  } catch (err) {
    throw err;
  }
};

// prettier-ignore
export const unFollowUserById = async (unFollowId) => {
  try {
    await postRequest(API_ENDPOINTS.USER + "/unfollow/" + unFollowId,{},true,false);
    // console.log(response);
  } catch (err) {
    throw err;
  }
};

export const getAllUsers = async () => {
  try {
    let response = await getRequest(API_ENDPOINTS.USER);

    return response.result;
  } catch (err) {
    throw err;
  }
};
