import React, { useEffect, useState } from "react";
import {
  MDBCard,
  MDBCardImage,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBBtn,
} from "mdb-react-ui-kit";
import { NavLink } from "react-router-dom";
import { Row, Col } from "react-bootstrap";
import ActionButtons from "./actionButto";

const PostCard = ({ data, type, isAuthor = false, deleteFunc }) => {
  const [frontContent, setFrontContent] = useState();
  const getText = () => {
    let parser = document.createElement("div");
    parser.innerHTML = data.content;

    setFrontContent(parser.textContent.substring(0, 70));
  };

  const sendDeletePost = () => {
    deleteFunc(data._id);
  };

  useEffect(() => {
    getText();
  }, []);
  return (
    <MDBCard style={{ height: "550px" }}>
      <MDBCardImage
        src={data.image}
        alt="..."
        position="top"
        style={{ height: "357px", width: "634px" }}
        className="img-fluid"
      />
      <MDBCardBody>
        <MDBCardTitle>{data.title}</MDBCardTitle>
        <MDBCardText>
          {/* <i>{"By: " + data.authorName + `(${data.date})`}</i> */}
          <i>
            By: <NavLink to={"/profile/@" + data.authorName}>{data.authorName}</NavLink> (
            {data.date})
          </i>
        </MDBCardText>
        <MDBCardText>{frontContent + "..............."}</MDBCardText>

        <Row>
          <Col>
            <NavLink to={"/posts/" + data.slug}>
              <MDBBtn>Read More</MDBBtn>
            </NavLink>
          </Col>
          <Col className="mt-2">
            {isAuthor && (
              <ActionButtons
                id={data._id}
                onDeleteClick={sendDeletePost}
                updatePath={"/author/posts/" + data.authorName + "/" + data._id}
              />
            )}
          </Col>
        </Row>
      </MDBCardBody>
    </MDBCard>
  );
};

export default PostCard;
