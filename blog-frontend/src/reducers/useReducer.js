export const initialState = JSON.parse(localStorage.getItem("authUser")) ? true : false;
export const initialStarState = null;
export const initialFollowState = null;

export const reducer = (state, action) => {
  if (action.type === "USER") {
    return action.payload;
  }

  return state;
};

export const starReducer = (state, action) => {
  if (action.type === "STAR") {
    return action.payload;
  }
  return state;
};

export const followReducer = (state, action) => {
  if (action.type === "FOLLOW") {
    // console.log(action.payload);
    return action.payload;
  }
  return state;
};
