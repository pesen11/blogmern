const router = require("express").Router();
const authMiddleware = require("../App/Middleware/authMiddleware");
const rbacMiddleware = require("../App/Middleware/rbacMiddleware");
const uploader = require("../App/Middleware/uploadMiddleware");
const PostController = require("../App/Controllers/postController");
const postCtrlObj = new PostController();
const cloudinary = require("../Config/cloudinary");

let setDestination = (req, res, next) => {
  req.dest = "post";
  next();
};

router.route("/").get(postCtrlObj.getAllPosts);

router
  .route("/addPost")
  .post(
    setDestination,
    uploader.single("image"),
    authMiddleware.loginCheck,
    rbacMiddleware.isAuthor,
    postCtrlObj.addPost
  );

router.route("/uploadImage").post(setDestination, uploader.single("upload"), async (req, res) => {
  try {
    if (req.file) {
      const result = await cloudinary.uploader.upload(req.file.path);
      res.json({
        uploaded: "true",
        url: result.secure_url,
      });
    }
  } catch (err) {
    console.log(err);
  }
});

router
  .route("/:id")
  .get(postCtrlObj.getPostbyId)
  .delete(authMiddleware.loginCheck, rbacMiddleware.isAdminAuthor, postCtrlObj.deletePostById)
  .put(
    setDestination,
    uploader.single("image"),
    authMiddleware.loginCheck,
    rbacMiddleware.isAdminAuthor,
    postCtrlObj.updatePost
  );

router
  .route("/:id/toggleStar")
  .post(authMiddleware.loginCheck, rbacMiddleware.isAdminAuthor, postCtrlObj.toggleStars);

router.route("/read/:slug").get(postCtrlObj.getPostBySlug);

module.exports = router;
