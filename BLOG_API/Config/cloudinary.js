const cloudinary = require("cloudinary").v2;

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY.API_KEY,
  api_secret: process.env.CLOUDINARY.API_SECRET,
});

module.exports = cloudinary;
